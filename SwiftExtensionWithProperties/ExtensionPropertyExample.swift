//
//  ExtensionPropertyExample.swift
//  SwiftExtensionWithProperties
//
//  Created by Andrew Benson on 2/25/18.
//  Copyright © 2018 Nuclear Cyborg Corp. All rights reserved.
//

import Foundation
import UIKit

class ExtensionPropertyExample {
    init() {
        print("Did init.")
    }
}


extension ExtensionPropertyExample {
    private struct theAnswer {
        static var name: String = ""
        static var city: String = ""
    }
    
    var name: String {
        get {
            guard let theName = objc_getAssociatedObject(self, &theAnswer.name) as? String else {
                return ""
            }
            return theName
        }
        set {
            objc_setAssociatedObject(self, &theAnswer.name, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var city: String {
        get {
            guard let theCity = objc_getAssociatedObject(self, &theAnswer.city) as? String else {
                return ""
            }
            return theCity
        }
        set {
            objc_setAssociatedObject(self, &theAnswer.city, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
}
