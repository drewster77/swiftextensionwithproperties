//
//  ViewController.swift
//  SwiftExtensionWithProperties
//
//  Created by Andrew Benson on 2/25/18.
//  Copyright © 2018 Nuclear Cyborg Corp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let a = ExtensionPropertyExample()
        let b = ExtensionPropertyExample()
        
        a.name = "Hank"
        a.city = "Chicago"
        b.name = "Mary"
        b.city = "New York"
        
        print("a name = \(a.name), city = \(a.city)")
        print("b name = \(b.name), city = \(b.city)")
        
        a.name = "Diane"
        a.city = "Sacramento"
        b.name = "Carl"
        b.city = "Minneapolis"
        
        print("a name = \(a.name), city = \(a.city)")
        print("b name = \(b.name), city = \(b.city)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}



