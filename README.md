Swift 4 doesn't support stored properties in extensions.

So here's what we do.

In your extension, define a private struct with the fields you're going to want.  Make everything static, like this:

    private struct theAnswer {
        static var name: String = ""
    }

I know, I know, static means it's a class-wide variable/property, not an instance one.  But we are not actually going to store anything in this struct.

In the code below, **obj_getAssociatedObject** and **objc_setAssociatedObject** both require an **UnsafeRawPointer** as a key.  We use these functions to store a key/value pair which is associated with this unique instance.

Here's the complete example with a String:


    import Foundation

    class ExtensionPropertyExample {

        // whatever
    }


    extension ExtensionPropertyExample {
        private struct theAnswer {
            static var name: String = ""
        }
    
        var name: String {
            get {
                guard let theName = objc_getAssociatedObject(self, &theAnswer.name) as? String else {
                    return ""
                }
                return theName
            }
            set {
                objc_setAssociatedObject(self, &theAnswer.name, newValue, .OBJC_ASSOCIATION_RETAIN)
            }
        }
    }

That's it!

